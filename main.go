package main

import (
	"encoding/json"
	"github.com/joho/godotenv"
	"io/ioutil"
	"log"
	"net/http"
	"time"
    "os"
)

type UnsplashClient struct {
	AuthorizationKey string
	DownloadFolder   string
}

type Download struct {
    Url string `json:"url"`
}
type RandomPhotoLinks struct {
	DownloadLocation string `json:"download_location"`
}
type RandomPhoto struct {
	Id        string           `json:"id"`
	CreatedAt time.Time        `json:"created_at"`
	UpdatedAt time.Time        `json:"updated_at"`
	Width     int              `json:"width"`
	Height    int              `json:"height"`
	Links     RandomPhotoLinks `json:"links"`
}

func (client *UnsplashClient) getRequestResponse(url string) (*http.Response, error) {

	httpClient := &http.Client{}

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
        log.Println("Error downloading %s", url)
		return nil, err
	}

	req.Header.Set("Authorization", "Client-ID " + client.AuthorizationKey)

	res, err := httpClient.Do(req)

	return res, err

}

// Uses unsplash api to get a random photo an return it
func (client *UnsplashClient) getRandomPhoto() RandomPhoto {

	res, err := client.getRequestResponse("https://api.unsplash.com/photos/random")
	if err != nil {
		log.Fatal(err)
	}
	defer res.Body.Close()

	photo := RandomPhoto{}

	body, err := ioutil.ReadAll(res.Body)
    log.Println("%s", string(body))

	if err != nil {
		log.Fatal(err)
	}

	err = json.Unmarshal(body, &photo)
	if err != nil {
		log.Fatal(err)
	}

    log.Printf("%v", photo)
	return photo
}

//Downloads the image and returns the image path
func (client *UnsplashClient) downloadPhoto(photo RandomPhoto) string {
	res, err := client.getRequestResponse(photo.Links.DownloadLocation)

	if err != nil {
		log.Fatal(err)
	}

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)

	if err != nil {
		log.Fatal(err)
	}
    download := Download{}

    err = json.Unmarshal(body, &download)

    imageResponse, err := client.getRequestResponse(download.Url)

    if err != nil {
        log.Fatal(err)
    }

    defer imageResponse.Body.Close()
    
	imageBody, err := ioutil.ReadAll(imageResponse.Body)


	path := client.DownloadFolder + "/" + photo.Id + ".jpg"

	err = ioutil.WriteFile(path, imageBody, 0644)

	if err != nil {
		log.Fatal(err)
	}

	return path
}

func main() {
	godotenv.Load()

    unsplashClient := &UnsplashClient{AuthorizationKey: os.Getenv("ACCESS_KEY"), DownloadFolder: os.Getenv("DOWNLOAD_FOLDER")}

    photo := unsplashClient.getRandomPhoto()
    path := unsplashClient.downloadPhoto(photo)

    log.Println("Downloaded at: %s", path)


}
